import { Injectable } from '@nestjs/common';

export type User = any;

@Injectable()
export class UsersService {
  private readonly users: User[];

  constructor() {
    this.users = [];
  }

  create(token, profile) {
    const index = this.users.push({ token, ...profile });
    const user = this.users[index - 1];
    return user;
  }

  findOne(id: number): Promise<User | undefined> {
    const user = this.users.find(user => user.id == id);

    return user;
  }
}
