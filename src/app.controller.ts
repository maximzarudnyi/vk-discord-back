import {
  Get,
  Controller,
  Header,
  Query,
  Req,
  Res,
  Param,
} from '@nestjs/common';
import { UsersService } from './users/users.service';
import { VK } from 'vk-io';

@Controller()
export class AppController {
  constructor(private usersService: UsersService) {}

  @Get('user')
  root(): string {
    return 'success';
  }

  @Get('status/:id')
  @Header('Cache-Control', 'none')
  @Header('Access-Control-Allow-Origin', '*')
  async userStatus(@Param('id') id) {
    const user = await this.usersService.findOne(id);
    const token = user.token;

    // console.log('userStatus user', user);

    const vk = new VK({
      token,
    });

    const res = await vk.api.status.get({
      user_id: user.id,
    });

    // console.log(res);

    return { ...res, date: new Date().getTime() };
  }
}
